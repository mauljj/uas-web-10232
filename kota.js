function tampilKota(propinsi)
{
    var kota = "";
    switch (propinsi)
    {
        case "Jabar" : 
            {
                 kota = "<ul> \
                        <li>Bandung</li> \
                        <li>Bogor</li> \
                        <li>Garut</li> \
                    </ul>"; 
             } break;
        case "Jateng" : 
            {
                kota = "<ul> \
                        <li>Cilacap</li> \
                        <li>Jepara</li> \
                        <li>Magelang</li> \
                    </ul>"; 
            } break;
        case "Jatim" : 
            {
                kota = "<ul> \
                        <li>Surabaya</li> \
                        <li>Lamongan</li> \
                        <li>Gresik</li> \
                    </ul>"; 
            } break;
            default : kota = "";
    }
    document.getElementById('kota').innerHTML = kota;
}
function gantiWarna()
{
    var pesan = document.getElementById('pesan');
    var button = document.getElementById('buttonWarna');
    if(button.innerHTML=="Hijau")
    {
        pesan.style.cssText = "background-color : green;";
        button.innerHTML = "Merah";
    }
    else if(button.innerHTML=="Merah")
    {
        pesan.style.cssText = "background-color : red;";
        button.innerHTML = "Hijau";
    }
}